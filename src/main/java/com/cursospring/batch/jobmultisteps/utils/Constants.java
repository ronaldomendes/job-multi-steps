package com.cursospring.batch.jobmultisteps.utils;

public class Constants {
    public static final String CONTEXT_KEY_NAME = "fileName";
    public static final String FILE_NAME_CSV = "employees.csv";
    public static final String FILE_NAME_OUTPUT_CSV = "output/employee_output.csv";
    public static final String EMPLOYEE_ID = "employeeId";
    public static final String FIRSTNAME = "firstName";
    public static final String LASTNAME = "lastName";
    public static final String EMAIL = "email";
    public static final String AGE = "age";
    public static final String STEP_ONE = "Saving_CSV_Data_Into_Database";
    public static final String STEP_TWO = "Writing_DB_Data_On_CSV_File";
    public static final String STEP_THREE = "Sending_Email_To_Employees";
    public static final String QUALIFIER_NAME = "demoJob";
    public static final int CHUNK_SIZE = 10;

    private Constants() {
    }
}
