package com.cursospring.batch.jobmultisteps.utils;

public class SqlScripts {
    public static final String ALL_EMPLOYEES = "select * from tb_employee";
    public static final String NEW_EMPLOYEE = "insert into tb_employee (employee_id, first_name, last_name, email, age) values (:employeeId, :firstName, :lastName, :email, :age)";

    private SqlScripts() {
    }
}
