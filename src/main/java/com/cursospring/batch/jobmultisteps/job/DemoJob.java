package com.cursospring.batch.jobmultisteps.job;

import com.cursospring.batch.jobmultisteps.dto.EmployeeDTO;
import com.cursospring.batch.jobmultisteps.mapper.EmployeeDBRowMapper;
import com.cursospring.batch.jobmultisteps.mapper.EmployeeFileRowMapper;
import com.cursospring.batch.jobmultisteps.model.Employee;
import com.cursospring.batch.jobmultisteps.processor.EmployeeProcessor;
import com.cursospring.batch.jobmultisteps.writer.EmailSenderWriter;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;

import static com.cursospring.batch.jobmultisteps.utils.Constants.AGE;
import static com.cursospring.batch.jobmultisteps.utils.Constants.CHUNK_SIZE;
import static com.cursospring.batch.jobmultisteps.utils.Constants.EMAIL;
import static com.cursospring.batch.jobmultisteps.utils.Constants.EMPLOYEE_ID;
import static com.cursospring.batch.jobmultisteps.utils.Constants.FILE_NAME_OUTPUT_CSV;
import static com.cursospring.batch.jobmultisteps.utils.Constants.FIRSTNAME;
import static com.cursospring.batch.jobmultisteps.utils.Constants.LASTNAME;
import static com.cursospring.batch.jobmultisteps.utils.Constants.QUALIFIER_NAME;
import static com.cursospring.batch.jobmultisteps.utils.Constants.STEP_ONE;
import static com.cursospring.batch.jobmultisteps.utils.Constants.STEP_THREE;
import static com.cursospring.batch.jobmultisteps.utils.Constants.STEP_TWO;
import static com.cursospring.batch.jobmultisteps.utils.SqlScripts.ALL_EMPLOYEES;
import static com.cursospring.batch.jobmultisteps.utils.SqlScripts.NEW_EMPLOYEE;

@Configuration
@AllArgsConstructor
public class DemoJob {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final EmployeeProcessor employeeProcessor;
    private final DataSource dataSource;

    // creating a new bean that will process three different steps
    @Qualifier(value = QUALIFIER_NAME)
    @Bean
    public Job demoOneJob() throws Exception {
        return this.jobBuilderFactory.get(QUALIFIER_NAME)
                .start(stepOneDemo()) // reading the csv file and save into database
                .next(stepTwoDemo()) // writing h2 data and save into csv file
                .next(stepThreeDemo()) // simulating an email sending
                .build();
    }

    private Step stepOneDemo() throws Exception {
        return stepBuilderFactory.get(STEP_ONE)
                .<EmployeeDTO, Employee>chunk(CHUNK_SIZE)
                .reader(employeeReader())
                .writer(employeeDBWriterDefault())
                .processor(employeeProcessor)
                .build();
    }

    private Step stepTwoDemo() throws Exception {
        return stepBuilderFactory.get(STEP_TWO)
                .<Employee, EmployeeDTO>chunk(CHUNK_SIZE)
                .reader(employeeDBReader())
                .writer(employeeFileWriter())
                .build();
    }

    private Step stepThreeDemo() throws Exception {
        return stepBuilderFactory.get(STEP_THREE)
                .<Employee, EmployeeDTO>chunk(CHUNK_SIZE)
                .reader(employeeDBReader())
                .writer(emailSenderWriter())
                .build();
    }

    @Bean
    @StepScope
    public Resource inputFileResource(@Value("#{jobParameters[fileName]}") final String fileName) throws Exception {
        return new ClassPathResource(fileName);
    }

    @Bean
    @StepScope
    public FlatFileItemReader<EmployeeDTO> employeeReader() throws Exception {
        FlatFileItemReader<EmployeeDTO> reader = new FlatFileItemReader<>();
        reader.setResource(inputFileResource(null));
        reader.setLineMapper(defineMapper());
        return reader;
    }

    private DelimitedLineTokenizer defineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(EMPLOYEE_ID, FIRSTNAME, LASTNAME, EMAIL, AGE);
        // a adição do delimitador em vírgula é opcional, por padrão já é dessa forma
        lineTokenizer.setDelimiter(DelimitedLineTokenizer.DELIMITER_COMMA);
        return lineTokenizer;
    }

    private DefaultLineMapper<EmployeeDTO> defineMapper() {
        DefaultLineMapper<EmployeeDTO> lineMapper = new DefaultLineMapper<>();
        lineMapper.setLineTokenizer(defineTokenizer());
        lineMapper.setFieldSetMapper(new EmployeeFileRowMapper());
        return lineMapper;
    }

    @Bean
    public JdbcBatchItemWriter<Employee> employeeDBWriterDefault() {
        JdbcBatchItemWriter<Employee> itemWriter = new JdbcBatchItemWriter<>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql(NEW_EMPLOYEE);
        itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        return itemWriter;
    }

    @Bean
    public ItemStreamReader<Employee> employeeDBReader() {
        JdbcCursorItemReader<Employee> reader = new JdbcCursorItemReader<>();
        reader.setDataSource(dataSource);
        reader.setSql(ALL_EMPLOYEES);
        reader.setRowMapper(new EmployeeDBRowMapper());
        return reader;
    }

    @Bean
    public ItemWriter<EmployeeDTO> employeeFileWriter() throws Exception {
        final Resource outputResource = new FileSystemResource(FILE_NAME_OUTPUT_CSV);
        FlatFileItemWriter<EmployeeDTO> writer = new FlatFileItemWriter<>();
        writer.setResource(outputResource);
        writer.setLineAggregator(defineLineAggregator());
        writer.setShouldDeleteIfExists(true);
        return writer;
    }

    private DelimitedLineAggregator<EmployeeDTO> defineLineAggregator() {
        DelimitedLineAggregator<EmployeeDTO> aggregator = new DelimitedLineAggregator<>();
        aggregator.setFieldExtractor(defineFieldExtractor());
        return aggregator;
    }

    private BeanWrapperFieldExtractor<EmployeeDTO> defineFieldExtractor() {
        BeanWrapperFieldExtractor<EmployeeDTO> extractor = new BeanWrapperFieldExtractor<>();
        extractor.setNames(new String[]{EMPLOYEE_ID, FIRSTNAME, LASTNAME, EMAIL, AGE});
        return extractor;
    }

    @Bean
    public EmailSenderWriter emailSenderWriter() {
        return new EmailSenderWriter();
    }
}
