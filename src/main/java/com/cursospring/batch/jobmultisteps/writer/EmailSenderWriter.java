package com.cursospring.batch.jobmultisteps.writer;

import com.cursospring.batch.jobmultisteps.dto.EmployeeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

@Slf4j
public class EmailSenderWriter implements ItemWriter<EmployeeDTO> {

    @Override
    public void write(List<? extends EmployeeDTO> employees) throws Exception {
        log.info("Email sent successfully to {} employees!", employees.size());
    }
}
