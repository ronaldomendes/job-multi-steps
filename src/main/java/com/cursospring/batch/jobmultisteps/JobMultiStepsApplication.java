package com.cursospring.batch.jobmultisteps;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.jobmultisteps"})
public class JobMultiStepsApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobMultiStepsApplication.class, args);
    }

}
