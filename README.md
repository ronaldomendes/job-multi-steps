# Spring Batch: Job Multi Steps

In this example you will learn how to create a job with multiple steps using Spring Batch architecture.

### The following example of this project is:
- Saving CSV data into a database
- Reading database data and export into CSV file
- "Sending" an email to employees 
